import { app, BrowserWindow } from "electron";
import staticServer from 'node-static'
import * as http from 'http';
import * as path from "path";

// Handle creating/removing shortcuts on Windows when installing/uninstalling.
if (require('electron-squirrel-startup')) { // eslint-disable-line global-require
    app.quit();
}

const file = new staticServer.Server(path.join(__dirname, "../public"), { cache: 0 })

require('http').createServer(function (request: http.IncomingMessage, response: http.ServerResponse) {
    request.addListener('end', function () {
        file.serve(request, response)
    }).resume()
}).listen(31337, "127.0.0.1");

function createWindow() {
    // Create the browser window.
    const mainWindow = new BrowserWindow({
        title         : "Morphcast",
        frame         : true,
        fullscreen    : false,
        webPreferences: {        /// <-- update this option
            nodeIntegration : true,
            contextIsolation: false,
            // preload: path.join(__dirname, "preload.js"),
        }
    });
    
    // and load the index.html of the app.
    //mainWindow.loadFile(path.join(__dirname, "../src/index.html"));
    mainWindow.loadURL('http://localhost:31337/');
    
    // Open the DevTools.
    mainWindow.webContents.openDevTools();
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on("ready", () => {
    createWindow();
});

app.on("activate", function () {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (BrowserWindow.getAllWindows().length === 0) createWindow();
});

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on("window-all-closed", () => {
    if (process.platform !== "darwin") {
        app.quit();
    }
});

// In this file you can include the rest of your app"s specific main process
// code. You can also put them in separate files and require them here.