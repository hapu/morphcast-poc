import { desktopCapturer } from 'electron'
export = class CameraStream {
    
    private video: HTMLVideoElement
    
    constructor(video: HTMLVideoElement) {
        this.video = video
    }
    
    initCamera(cb: CallableFunction){   
        desktopCapturer
        .getSources({ types: ['window', 'screen'] })
        .then(async sources => {
            for (const source of sources) {
                if (source.name === 'Morphcast') {
                    try {
                        const stream = await navigator.mediaDevices.getUserMedia({
                            audio: false,
                            video: true
                        })
                        
                        this.video.srcObject        = stream
                        this.video.onloadedmetadata = (event) => {
                            this.video.play()
                            cb()
                        }
                    } catch (error) {
                        console.log(error)
                    }
                    return
                }
            }
        })
    }
}