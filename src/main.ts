import {Morphcast, PersonInterface } from './Morphcast'
import CameraStream from './cameraStream'
import * as dotenv from "dotenv-flow";
dotenv.config();

if (!process.env.MP_KEY) {
    throw new Error("MP_KEY is not valid. Please populate it in .env file");
} 

const video         = <HTMLVideoElement> document.getElementById('video')
const quitBtn       = <HTMLInputElement> document.getElementById('quit')
const skipAllErrors = <HTMLInputElement> document.getElementById('skipAllErrors')
const table         = <HTMLTableElement> document.getElementById('morphcast-logs')

const morphcast = new Morphcast(window, video, process.env.MP_KEY!)
const camera    = new CameraStream(video)

skipAllErrors.onchange = function(){
    morphcast.stop()
    morphcast.start(skipAllErrors.checked)
}

let analysisCounter = 0
morphcast.on('image-analyzed', function(event){
    
    table.scrollTop = table.scrollHeight;
    
    let row           = table.insertRow()
    let analysisIndex = row.insertCell()
    let time      = row.insertCell()
    let logs          = row.insertCell()
        logs.colSpan  = 5
    let logTable      = document.createElement("table")
    logs.appendChild(logTable)
    
    analysisIndex.appendChild(document.createTextNode(String(analysisCounter++)))
    time.appendChild(document.createTextNode(new Date().toLocaleString().split(",")[1]))
    
    event.forEach((person: PersonInterface, index:number) => {
        
        let rowLog      = logTable.insertRow()
        let personIndex = rowLog.insertCell()
        let age         = rowLog.insertCell()
        let gender      = rowLog.insertCell()
        let emotion     = rowLog.insertCell()
        let features    = rowLog.insertCell()
        
        personIndex.appendChild(document.createTextNode(String(index+1)))
        age.appendChild(document.createTextNode(String(person.age)))
        gender.appendChild(document.createTextNode(String(person.gender)))
        emotion.appendChild(document.createTextNode(String(person.emotion)))
        features.appendChild(document.createTextNode("[" + String(person.features?.join(", "))+"]"))
    })
    
});

function sessionStart() {
    morphcast.start(skipAllErrors.checked)
    quitBtn.textContent = "stop"
    quitBtn.onclick     = sessionEnd
}

function sessionEnd() {
    morphcast.stop()
    quitBtn.textContent = "start"
    quitBtn.onclick     = sessionStart
}

camera.initCamera(sessionStart)