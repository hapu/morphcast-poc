import EventEmitter from 'events'

declare global {
    interface Window { CY: any; }
}

interface PersonInterface {
    age?    : Number | String,
    gender? : String,
    emotion?: String,
    features? : String[]
}

const loadAPICallbacks: CallableFunction[] = []
class Morphcast extends EventEmitter {
    
    private video        : HTMLVideoElement
    private key          : String
    private customSource : CustomSource
    private cameraSource : CameraSource
    private morphcastSDK : any
    private morphcastAPI : any
    private otherFaces   : ImageData[] = []
    private people       : PersonInterface[] = []
    private skipAllErrors: Boolean = false
    
    constructor(window: Window, video: HTMLVideoElement, key:string){
        super()
        this.video = video
        this.key   = key
        
        this._loadAPI((err: any, CY: any) => {
            if (err) return this.destroy(new Error('Morphcast API failed to load'))
            this.morphcastAPI = CY
        })
        
        this.cameraSource = new CameraSource(this.morphcastAPI, this.video)
        this.customSource = new CustomSource(this.cameraSource)
        this.morphcastSDK = new Promise((res) => {
            res(this.morphcastAPI.loader()
            .licenseKey(this.key)
            .source(this.customSource)
            .addModule(this.morphcastAPI.modules().FACE_DETECTOR.name, {multiFace: true})
            .addModule(this.morphcastAPI.modules().FACE_AGE.name)
            .addModule(this.morphcastAPI.modules().FACE_GENDER.name, {smoothness: 0.0, threshold: 0.50})
            .addModule(this.morphcastAPI.modules().FACE_EMOTION.name, {smoothness: 0, enableBalancer : false})
            .addModule(this.morphcastAPI.modules().FACE_FEATURES.name, {smoothness: 0.90})
            .addModule(this.morphcastAPI.modules().FACE_AROUSAL_VALENCE.name, {smoothness: 0})
            .addModule(this.morphcastAPI.modules().FACE_ATTENTION.name, {smoothness: 0})
            .addModule(this.morphcastAPI.modules().FACE_WISH.name, {smoothness: 0})
            //   .addModule(this.morphcastAPI.modules().ALARM_LOW_ATTENTION.name, config)
            .load());
        });
        
        window.addEventListener(this.morphcastAPI.modules().EVENT_BARRIER.eventName, (event) => this._handleEvent(event))
    }
    
    public start(skipAllErrors: boolean){
        this.skipAllErrors = skipAllErrors
        console.info("starting MorphCast SDK, skip all errors: ", this.skipAllErrors)
        this.morphcastSDK.then( ({start}:{start: CallableFunction}) => start());
        this.customSource.analyzeFrame(this.cameraSource.getSource().getFrame(640))
    } 
    
    public stop(){
        this.morphcastSDK.then(({stop}:{stop: CallableFunction}) => stop());
    }
    
    public destroy(err: Error) {
        if (err) this.emit('error', err)
        this.morphcastSDK.then(({terminate}:{terminate: CallableFunction}) => terminate());
    }
    
    private _handleEvent(event: any) {
        
        console.debug("Raw log: ", event)
        
        // Save main person in pic
        let person: PersonInterface = {
            age    : event.detail.face_age ? event.detail.face_age.numericAge || null             : "module error",
            gender : event.detail.face_gender ? event.detail.face_gender.mostConfident || null    : "module error",
            emotion: event.detail.face_emotion ? event.detail.face_emotion.dominantEmotion || null: "module error",
            features: event.detail.face_features ? 
                Object.keys(event.detail.face_features.features).filter( key => event.detail.face_features.features[key] > 0.5 ) || [] : ["module error"] 
        }
        
        if  (this.skipAllErrors && 
            ((person.age != "module error" && person.age != null) || 
            (person.gender != "module error" && person.gender != null) ||
            (person.emotion != "module error" && person.emotion != null))
            ) {
                this.people.push(person)
            } 
        else if (!this.skipAllErrors) {
                this.people.push(person)
        }
            
        // Evaluate if there are other ppl 
        const faces:ImageData[] = event.detail.face_detector ? event.detail.face_detector.faces : [];
        if (faces.length > 1) {
            this.otherFaces = faces.slice(1)
        }
        
        // if face-buff not empty analyze face, otherwise send event and grab new frame from camera
        if (this.otherFaces.length) {
            this.customSource.analyzeFrame(this.otherFaces.shift()!)        
        } else {
            this._sendEvent()
            this.customSource.analyzeFrame(this.cameraSource.getSource().getFrame(640))
        }
    }
        
    private _sendEvent(){
        if (this.people.length > 0) {
            console.debug("Frame Analyzed: ", this.people)
            this.emit('image-analyzed', this.people)
            this.people = []
        } else {
            console.error("Analysis done with bad data")
        }
    }
    
    private _loadAPI (cb: CallableFunction) {
        // If API is loaded, there is nothing else to do
        if (window.CY && typeof window.CY.loader === 'function') {
            return cb(null, window.CY)
        }
        
        loadAPICallbacks.push(cb)
        
        while (loadAPICallbacks.length) {
            const loadCb = loadAPICallbacks.shift()
            
            if (loadCb != undefined){
                loadCb(null, window.CY)
            } 
        }
    }
}

    
declare class MorphcastCamera {
    constructor()
    getFrame(maxSize?: Number): ImageData  // Promise<ImageData>
    start    ()               : void
    stop     ()               : void
                height           : Number
                width            : Number
                stopped          : boolean
                flip             : Number
}

class CameraSource {
    
    private video       : HTMLVideoElement
    private morphcastAPI: any
    private source      : any
    
    constructor(api: any, video: HTMLVideoElement) {
        this.morphcastAPI = api
        this.video        = video
    }
    
    public getSource(): MorphcastCamera {
        
        if (!this.source) {
            this.source = this.morphcastAPI.createSource.fromVideoElement(this.video)
        }
        
        return this.source
    }
}

class CustomSource {
    
    private crtImgData  : ImageData | null;
    private resolver    : CallableFunction | null;
    private cameraSource: CameraSource
    
    constructor(cameraSource: CameraSource) {
        this.crtImgData   = null
        this.resolver     = null
        this.cameraSource = cameraSource
    }
    
    public analyzeFrame(imageData: ImageData) {
        if (this.resolver) {
            this.resolver(imageData)
            this.resolver = null
        } else {
            this.crtImgData = imageData
        }
    }
    
    public getFrame(): Promise<ImageData> {
        if (this.crtImgData) {
            const p               = Promise.resolve(this.crtImgData)
                    this.crtImgData = null
            
            return p;
        } else {
            
            return new Promise(res => this.resolver = res)
        }
    }
    
    public start() { 
        return this.cameraSource.getSource().start()
    }
    
    public stop() { 
        return this.cameraSource.getSource().stop()
    }
    
    public get stopped() { 
        return this.cameraSource.getSource().stopped
    }
} 

export {
    Morphcast,
    PersonInterface
} 